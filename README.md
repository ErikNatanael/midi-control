MIDI control
============

This crate is meant to provide a high level API for dealing with MIDI
controllers, mostly decoding MIDI data received and encoding MIDI data
sent.

What it solves?
---------------

For another project in Rust, a software musical instrument, I needed
code to decode the byte buffers coming from the MIDI device, to
respond to the events of the MIDI controller I have. I didn't find any
crate I liked or that had the feature I wanted. I found `midir` for
the "transport" layer, ie communicating with the device. So I wrote
something and it worked.

Later I wanted to play with the controler and found out the
proprietary MIDI commands to configure it. On a broader scope I wanted
to see how I could approach configuring MIDI controller for use as a
musical instrument, something vendor agnostic. So I started write some
ugly code and then started this crate with what I had previously
written.

This library will take byte buffers and decode them into a structured
data. And will turn one of these structured data to a buffer of
bytes. It will try to provide also functions to use the vendor MIDI
messages.

How do you do use this?
-----------------------

When you received data from the MIDI device in `buffer`.

```rust
let message = MidiMessage::from(buffer);
match command {
    MidiMessage::NoteOn(ch, e) =>
        /* note on  */,
    MidiMessage::NoteOff(ch, e) =>
        /* note off */
}
```

To send a command of MIDI:

```rust
let message = MidiMessage::NoteOn(Channel::Ch1, 60);
let buffer: Vec<u8> = message.into();
/* send the buffer to your MIDI device */
```

Where to start?
---------------

The example `arturia-blink.rs` will use `midir` to send commands to
blink the pads with cycling colours.

Add to your `Cargo.toml` the following dependency:

```toml
midi-control = "0.1.0"
```

I recommend reading about MIDI is you have no experience.

More
----

As of now only MIDI 1.0 is supported. This is the 1983 standard with
its improvement. MIDI 2.0 might come but right now is not the
priority.

As of today, only one vendor Sys Exclusive messages is supported and
is the one for Arturia™ controllers. This is not in anyway endorsed or
supported by them, it just happen to be the controller I have: an
inexpensive MiniLab MkII.

The crate doesn't provide API to the transport (MIDI driver), but by default
will build support for using `midir` as transport. Disable the
`transport` feature to build it standalone:
```
cargo build --no-default-features
```

Example programs require the `transport` feature.

Developer info
--------------

The source code is hosted on gitlab:

https://gitlab.com/hfiguiere/midi-control

Licensing
---------

License: LGPL-3.0-or-later
Author: Hubert Figuière <hub@figuiere.net>
