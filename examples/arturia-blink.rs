//
// (c) 2020 Hubert Figuière
//
// License: LGPL-3.0-or-later

//! This example will show how to blink the pad lights in a cycle on an
//! Arturia MiniLab MkII USB controller.
//!
//! The knowledge was acquired through reverse engineering.
//! It will probably not work with any other device, but could be adapted
//! to do so.

extern crate midi_control;
extern crate midir;

use std::sync::mpsc::channel;
use std::thread;
use std::time;

use midi_control::consts;
use midi_control::sysex::USysExDecoder;
use midi_control::vendor::arturia;
use midi_control::{MidiMessage, SysExEvent};

/// Print a message on error returned.
macro_rules! print_on_err {
    ($e:expr) => {
        if let Err(err) = $e {
            eprintln!(
                "{}:{} Error '{}': {}",
                file!(),
                line!(),
                stringify!($e),
                err
            );
        }
    };
}

/// String to look for when enumerating the MIDI devices
const ARTURIA_DEVICE: &str = "Arturia MiniLab mkII";

fn main() {
    let midi_input = midir::MidiInput::new("MIDITest").unwrap();

    let mut device_index: Option<usize> = None;
    let count = midi_input.port_count();
    for i in 0..count {
        if let Ok(port_name) = midi_input.port_name(i) {
            if port_name.contains(ARTURIA_DEVICE) {
                device_index = Some(i);
                break;
            }
        }
    }
    if device_index.is_none() {
        println!("Device not found!");
        return;
    }

    let (sender, receiver) = channel::<MidiMessage>();

    let device_index = device_index.unwrap();
    let _connect_in = midi_input.connect(
        device_index,
        ARTURIA_DEVICE,
        move |_timestamp, data, sender| {
            let msg = MidiMessage::from(data);
            // println!("{}: received {:?} => {:?}", timestamp, data, msg);
            print_on_err!(sender.send(msg));
        },
        sender,
    );
    let midi_output = midir::MidiOutput::new("MIDITest").unwrap();

    if let Ok(mut connect_out) = midi_output.connect(device_index, ARTURIA_DEVICE) {
        let msg = MidiMessage::SysEx(SysExEvent::new_non_realtime(
            consts::usysex::ALL_CALL,
            [0x06, 0x01],
            &[0xf7],
        ));
        let raw: Vec<u8> = msg.into();
        print_on_err!(connect_out.send(raw.as_slice()));
        println!("Press Control-C at anytime to stop the demo");

        let hundred_millis = time::Duration::from_millis(100);
        loop {
            if let Ok(msg) = receiver.recv() {
                if let Some(decoder) = USysExDecoder::decode(&msg) {
                    if decoder.is_non_realtime()
                        && decoder.target_device() == 0
                        && decoder.general_info_reply_manufacturer_id()
                            == Some(arturia::EXTENDED_ID_VALUE)
                    {
                        if decoder.general_info_reply_family() != Some(([2, 0], [4, 2])) {
                            println!("Your device isn't supported by this demo");
                            println!("Only the Arturia minilab MkII is supported");
                        } else {
                            run_demo(&mut connect_out);
                        }
                    }
                }
            }
            thread::sleep(hundred_millis);
        }
    }
}

fn run_demo(connect_out: &mut midir::MidiOutputConnection) {
    let delay = time::Duration::from_millis(100);

    // pads 1..8
    // for pads 9..16 use 0x78..=0x7b
    let pads = 0x70..0x78;

    // clear all pads.
    for pad in pads.clone() {
        let msg = MidiMessage::SysEx(SysExEvent::new_manufacturer(
            arturia::EXTENDED_ID_VALUE,
            &[0x7f, 0x42, 0x02, 0x00, 0x10, pad, 0x00, 0xf7],
        ));
        let raw: Vec<u8> = msg.into();
        print_on_err!(connect_out.send(raw.as_slice()));
    }

    let colour_set = [1u8, 4, 5, 16, 17, 20, 127];
    let mut iter = colour_set.iter();
    loop {
        for pad in pads.clone().rev() {
            let colour = if let Some(c) = iter.next() {
                c
            } else {
                iter = colour_set.iter();
                iter.next().unwrap()
            };
            let msg = arturia::v2::set_value(0x10, pad, *colour);
            let raw: Vec<u8> = msg.into();
            print_on_err!(connect_out.send(raw.as_slice()));
            thread::sleep(delay);
        }
        for pad in pads.clone().rev() {
            let msg = arturia::v2::set_value(0x10, pad, 0);
            let raw: Vec<u8> = msg.into();
            print_on_err!(connect_out.send(raw.as_slice()));
            thread::sleep(delay);
        }
        for pad in pads.clone() {
            let colour = if let Some(c) = iter.next() {
                c
            } else {
                iter = colour_set.iter();
                iter.next().unwrap()
            };
            let msg = arturia::v2::set_value(0x10, pad, *colour);
            let raw: Vec<u8> = msg.into();
            print_on_err!(connect_out.send(raw.as_slice()));
            thread::sleep(delay);
        }
        for pad in pads.clone() {
            let msg = arturia::v2::set_value(0x10, pad, 0);
            let raw: Vec<u8> = msg.into();
            print_on_err!(connect_out.send(raw.as_slice()));
            thread::sleep(delay);
        }
    }
}
