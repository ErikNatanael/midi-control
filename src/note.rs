//
// (c) 2020 Hubert Figuière
//
// License: LGPL-3.0-or-later

//! Utilities to handle MIDI notes.

/// A Note value in Midi
///
/// MIDI Note are simply 0..127
pub type MidiNote = u8;

/// Return if the midi_note is an accidental.
///
/// This can be used to determine if the note is a black key
/// on a piano style keyboard.
pub fn midi_is_accident(midi_note: MidiNote) -> bool {
    // C-1 is 0, C0 is 12, etc.
    // A simple calculation of the modulo is enough.
    match midi_note % 12 {
        1 | 3 | 6 | 8 | 10 => true,
        _ => false,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_midi_is_accident() {
        assert!(!midi_is_accident(36));
        assert!(midi_is_accident(37));
        assert!(!midi_is_accident(38));
        assert!(midi_is_accident(39));
        assert!(!midi_is_accident(40));
        assert!(!midi_is_accident(41));
    }
}
