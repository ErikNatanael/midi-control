//
// (c) 2020 Hubert Figuière
//
// License: LGPL-3.0-or-later

//! Arturia device support
//! This is done without any warranty or any documentation or support from the vendor
//! (albeit I'd love this to change)

use crate::sysex;

/// Arturia extended manufacturer ID
pub const EXTENDED_ID_VALUE: sysex::ManufacturerId = sysex::ManufacturerId::ExtId(32, 0x6b);

/// Arturia_v2 protocol
///
/// SysEx messages using EXTENDED_ID_VALUE
/// starts with 0x7f, FAMILY_ID
pub mod v2 {
    use super::EXTENDED_ID_VALUE;
    use crate::consts;
    use crate::message::{MidiMessage, SysExEvent};

    /// Define the product id for the command.
    /// This is defining the protocol.
    pub const PRODUCT_ID: u8 = 0x42;

    /// Verbs for the command
    pub mod verb {
        /// Used to query a value from the device.
        pub const GET: u8 = 0x01;
        /// Use to set a value on the device, or the device to tell the live.
        pub const SET: u8 = 0x02;
        /// Select the device memory to read.
        pub const READ_MEM: u8 = 0x05;
        /// Select the device memory to write to.
        pub const WRITE_MEM: u8 = 0x06;
    }

    /// Build a SysEx message to query a value.
    /// * param_id is the id on the item
    /// * item_id is a control (logical)
    pub fn get_value(param_id: u8, item_id: u8) -> MidiMessage {
        MidiMessage::SysEx(SysExEvent::new_manufacturer(
            EXTENDED_ID_VALUE,
            &[
                0x7f,
                PRODUCT_ID,
                verb::GET,
                0x00,
                param_id,
                item_id,
                0,
                consts::EOX,
            ],
        ))
    }

    /// Build a SysEx message to set a value.
    /// * param_id is the id on the item
    /// * item_id is a control (logical)
    /// * value the value
    pub fn set_value(param_id: u8, item_id: u8, value: u8) -> MidiMessage {
        MidiMessage::SysEx(SysExEvent::new_manufacturer(
            EXTENDED_ID_VALUE,
            &[
                0x7f,
                PRODUCT_ID,
                verb::SET,
                0x00,
                param_id,
                item_id,
                value,
                consts::EOX,
            ],
        ))
    }
}
